import axios from "axios";

import { API_BASE_URL } from "src/config";
export const POSTUserPostToDynamoDB = async (data, jwtToken) => {
  // console.log(data);
  // console.log(jwtToken);
  const url = `${API_BASE_URL}/post`;
  data = JSON.stringify(data);
  return new Promise((resolve, reject) => {
    const config = {
      method: "post",
      url: url,
      headers: {
        Authorization: jwtToken,
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      data,
    };

    // console.log("process.env", process.env);

    console.log("POSTUserPostToDynamoDB ", config);

    axios(config)
      .then((response) => {
        console.log("RESPONSE", response);
        resolve(resolve);
      })
      .catch((error) => {
        console.log(error);
        reject(error);
      });
  });
};

export const ApprovePost = async (data, jwtToken) => {
  // console.log(data);
  // console.log(jwtToken);
  const url = `${API_BASE_URL}/post/approve`;
  data = JSON.stringify(data);
  return new Promise((resolve, reject) => {
    const config = {
      method: "post",
      url: url,
      headers: {
        Authorization: jwtToken,
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      data,
    };

    // console.log("process.env", process.env);

    console.log("ApprovePost ", config);

    axios(config)
      .then((response) => {
        console.log("RESPONSE", response);
        resolve(resolve);
      })
      .catch((error) => {
        console.log(error);
        reject(error);
      });
  });
};

export const RejectPost = async (data, jwtToken) => {
  // console.log(data);
  // console.log(jwtToken);
  const url = `${API_BASE_URL}/post/reject`;
  data = JSON.stringify(data);
  return new Promise((resolve, reject) => {
    const config = {
      method: "post",
      url: url,
      headers: {
        Authorization: jwtToken,
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      data,
    };

    // console.log("process.env", process.env);

    console.log("RejectPost ", config);

    axios(config)
      .then((response) => {
        console.log("RESPONSE", response);
        resolve(resolve);
      })
      .catch((error) => {
        console.log(error);
        reject(error);
      });
  });
};

export const EditPostRequest = async (data, jwtToken) => {
  // console.log(data);
  // console.log(jwtToken);
  const url = `${API_BASE_URL}/post/edit`;
  data = JSON.stringify(data);
  return new Promise((resolve, reject) => {
    const config = {
      method: "post",
      url: url,
      headers: {
        Authorization: jwtToken,
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      data,
    };

    // console.log("process.env", process.env);

    console.log("EditPost ", config);

    axios(config)
      .then((response) => {
        console.log("RESPONSE", response);
        resolve(resolve);
      })
      .catch((error) => {
        console.log(error);
        reject(error);
      });
  });
};
