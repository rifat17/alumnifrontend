import { Client } from "elasticsearch";
import { API_BASE_URL, ES_USERNAME, ES_PASSWORD } from "src/config";

export const USER_LOADING = "USER_LOADING";
export const USER_LOADED = "USER_LOADED";
export const AUTH_ERROR = "AUTH_ERROR";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const REGISTRATION_SUCCESS = "REGISTRATION_SUCCESS";
export const REGISTRATION_FAIL = "REGISTRATION_FAIL";
export const REGISTRATION = "REGISTRATION";

// POST
// admin
export const ADMIN_ADD_NEW_POST = "ADMIN_ADD_NEW_POST";
export const ADMIN_APPROVE_POST = "ADMIN_APPROVE_POST";
export const ADMIN_REJECT_POST = "ADMIN_REJECT_POST";
export const POST_UPDATING = "POST_UPDATING";
export const POST_UPDATED_OK = "POST_UPDATING_OK";
export const ADD_APPROVED_POST = "ADD_APPROVED_POST";

//user
export const USER_PENDING_NEW_POST = "USER_PENDING_NEW_POST";

//              POST

//   /           |           \
// home     pending         approve

export const POST_LOADING_BEGIN = "POST_LOADING_BEGIN";
export const POST_LOADING_SUCCESS = "POST_LOADING_SUCCESS";
export const POST_LOADING_FAILED = "POST_LOADING_FAILED";

//home
export const ADD_POST_TO_HOME = "ADD_POST_TO_HOME";
export const REMOVE_POST_FROM_HOME = "REMOVE_POST_FROM_HOME";

// pending
export const ADD_POST_TO_PENDING = "ADD_POST_TO_PENDING";
export const REMOVE_POST_FROM_PENDING = "REMOVE_POST_FROM_PENDING";

// approval
export const ADD_POST_TO_APPROVAL = "ADD_POST_TO_APPROVAL";
export const REMOVE_POST_FROM_APPROVAL = "REMOVE_POST_FROM_APPROVAL";

export const INREVIEW = "INREVIEW";
export const APPROVED = "APPROVED";
export const REJECTED = "REJECTED";
export const DELETED = "DELETED";

export const INDEXES = {
  POST: "post",
  USER: "user",
};

const post_loading = () => ({
  type: POST_LOADING_BEGIN,
});

const post_loading_success = (posts) => ({
  type: POST_LOADING_SUCCESS,
  payload: {
    loaded: true,
    posts: posts,
  },
});

const post_loading_fail = (error) => ({
  type: POST_LOADING_FAILED,
  payload: error,
});

const client = new Client({
  host: "https://search-alumni-elasticsearch-3f4fkqhbwl5j3mluybnqlfopu4.us-east-2.es.amazonaws.com/",
  httpAuth: `${ES_USERNAME}:${ES_PASSWORD}`,
});

export function fetchPostsHOME(status, index, sortOrder = "desc") {
  return (dispatch) => {
    const response = client.search(
      {
        index: index,
        body: {
          query: {
            match: {
              post_status: status,
            },
          },
          sort: [
            {
              created_at: sortOrder,
            },
          ],
        },
      },
      {
        maxRetries: 3,
        ignore: [403],
      }
    );
    response
      .then((data) => {
        const fetchedposts = data.hits.hits.map((post) => post["_source"]);
        console.log(fetchedposts);
        dispatch(post_loading_success(fetchedposts));
        return fetchedposts;
      })
      .catch((err) => {
        console.log(err);
      });
  };
}

export function fetchPostsUSERPENDINGandDispatch(
  status,
  index,
  username,
  sortOrder = "desc"
) {
  return (dispatch) => {
    dispatch(post_loading());

    const response = client.search(
      {
        index: index,
        body: {
          query: {
            bool: {
              must: [
                {
                  match: {
                    post_status: status,
                  },
                },
                {
                  match: {
                    postOwner: username,
                  },
                },
              ],
            },
          },
        },
      },
      {
        maxRetries: 3,
        ignore: [403],
      }
    );
    response
      .then((data) => {
        const fetchedposts = data.hits.hits.map((post) => post["_source"]);
        console.log(fetchedposts);
        // dispatch({
        //   type: "LOAD_PENDING_POST",
        //   payload: fetchedposts,
        // });
        // debugger;
        return fetchedposts;
      })
      .catch((err) => {
        console.log(err);
      });
  };
}

export function fetchPostsUSERPENDING(
  status,
  index,
  username,
  sortOrder = "desc"
) {
  const response = client.search(
    {
      index: index,
      body: {
        query: {
          bool: {
            must: [
              {
                match: {
                  post_status: status,
                },
              },
              {
                match: {
                  postOwner: username,
                },
              },
            ],
          },
        },
      },
    },
    {
      maxRetries: 3,
      ignore: [403],
    }
  );

  response
    .then((data) => {
      const fetchedposts = data.hits.hits.map((post) => post["_source"]);
      console.log("fetchedposts", fetchedposts);
      // dispatch({
      //   type: "LOAD_PENDING_POST",
      //   payload: fetchedposts,
      // });
      return fetchedposts;
    })
    .catch((err) => {
      console.log(err);
    });
}
export function fetchPostsADMINAPPROVAL(status, index, sortOrder = "desc") {
  console.log("HII");
  return (dispatch) => {
    const response = client.search(
      {
        index: index,
        body: {
          query: {
            match: {
              post_status: status,
            },
          },
          sort: [
            {
              created_at: sortOrder,
            },
          ],
        },
      },
      {
        maxRetries: 3,
        ignore: [403],
      }
    );
    response
      .then((data) => {
        const fetchedposts = data.hits.hits.map((post) => post["_source"]);
        console.log("fetchPostsADMINAPPROVAL", fetchedposts);
        dispatch({
          type: ADD_POST_TO_APPROVAL,
          payload: {
            posts: fetchedposts,
            loaded: true,
          },
        });
        return fetchedposts;
      })
      .catch((err) => {
        console.log(err);
      });
  };
}

export function getPostByID(index, ID) {
  console.log("ID", ID);
  const q = JSON.stringify({
    query: {
      match: {
        ID: "zpNUaj7NOl5R-XYl_KVKw",
      },
    },
  });
  const response = client.search({
    index: index,
    body: q,
  });

  return response;
}
// JSON.stringify({
//   "query": {
//     "match": {
//       "ID": "zpNUaj7NOl5R-XYl_KVKw"
//     }
//   }
// })
