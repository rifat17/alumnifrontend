import {
  ADD_POST_TO_HOME,
  POST_LOADING_FAILED,
  POST_LOADING_SUCCESS,
} from "src/actions/types";
import postData from "src/views/pages/posts/PostsData";

// const approved_posts = [...postData];
const approved_posts = { loaded: false, posts: [] };

export default function (state = approved_posts, action) {
  switch (action.type) {
    case ADD_POST_TO_HOME:
      // return state;
      let newState = { posts: [...state.posts, action.payload] };
      // console.log("STATE", state);
      // console.log("NEWSTATE", newState);
      return { ...state, ...newState };
    case POST_LOADING_SUCCESS:
      console.log(action.payload);
      // const newState = { ...action.payload.loaded };
      return { ...state, ...action.payload };
    case POST_LOADING_FAILED:
      return [];
    case "REMOVE_POST_FROM_HOME":
      let newPosts = state.posts.filter(
        (post) => post.PK !== action.payload.PK
      );
      newState = { posts: [...newPosts] };
      return { ...state, ...newState };
    default:
      return state;
  }
}
