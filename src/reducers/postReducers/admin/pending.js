import {
  ADD_POST_TO_APPROVAL,
  REMOVE_POST_FROM_APPROVAL,
} from "src/actions/types";

const pending_posts = { loaded: false, posts: [] };

export default function (state = pending_posts, action) {
  switch (action.type) {
    case REMOVE_POST_FROM_APPROVAL:
      const newState = {
        posts: state.posts.filter((post) => post.PK !== action.PK),
      };
      return { ...state, ...newState };
    // return pending_posts.fillter((post) => post.PK !== action.PK);
    case ADD_POST_TO_APPROVAL:
      console.log(state);
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
