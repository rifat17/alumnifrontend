import {
  ADD_POST_TO_PENDING,
  REMOVE_POST_FROM_PENDING,
} from "src/actions/types";
import data from "./userPendingPostData";

const pending_posts = { loading: false };

export default function (state = pending_posts, action) {
  switch (action.type) {
    // case ADD_POST_TO_PENDING:
    //   // const { userPK } = action.payload;
    //   // const userPosts = state[userPK] || { [userPK]: [] };
    //   // state[userPK].push(action.payload.post);

    //   // // let aState = { ...state, [userPK]: newpostState };

    //   // // const newUserPosts = { ...userPosts, ...action.payload };
    //   // // console.log(newUserPosts);

    //   // return state;

    //   // case "LOAD_PENDING_POST":
    //   return [...action.payload, state];

    case REMOVE_POST_FROM_PENDING:
      // debugger;
      const posts = state[action.payload.userPK] || {
        [action.payload.userPK]: [],
      };
      const filterd = posts.filter((post) => post.PK !== action.payload.postPK);
      const newState = {
        [action.payload.userPK]: [...filterd],
      };

      return { ...state, ...newState };
    default:
      return state;
  }
}
