import { combineReducers } from "redux";

import homePagePost from "./posts";
import userPendingPost from "./user/pending";
import adminPendingPost from "./admin/pending";

export default combineReducers({
  homePagePost,
  approve: adminPendingPost,
  pending: userPendingPost,
});
