import { combineReducers } from "redux";
import authReducer from "./authReducer";
import sidebarReducer from "./sidebarReducer";
import postReducer from "./postReducers";

export default combineReducers({
  auth: authReducer,
  sidebarShow: sidebarReducer,
  post: postReducer,
});
