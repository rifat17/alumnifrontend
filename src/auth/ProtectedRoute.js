import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useSelector, connect } from "react-redux";

const ProtectedRoute = ({ component: Component, path, ...rest }) => {
  const authRedux = useSelector((state) => state.auth);
  const loggedIn = authRedux.isAuthenticated;

  // console.log("PATH ", path);
  // console.log("REST", rest);
  const { location } = rest;
  return (
    <Route
      {...rest}
      path={path}
      render={(props) => {
        return loggedIn ? (
          <Component {...rest} {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: {
                prevLocation: location.pathname,
                error: "You need to login first!",
              },
            }}
          />
        );
      }}
    />
  );
};

const mapStateToProps = (state) => {
  const { isAuthenticated: loggedIn } = state.auth;
  return {
    loggedIn,
  };
};

export default connect(mapStateToProps)(ProtectedRoute);
