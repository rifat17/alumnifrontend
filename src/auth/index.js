import { isAllowed, isAuthenticated, hasRole } from "./check_permission";

export { isAuthenticated, isAllowed, hasRole };
