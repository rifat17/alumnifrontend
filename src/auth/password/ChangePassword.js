import CIcon from "@coreui/icons-react";
import {
  CCard,
  CCardBody,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CButton,
  CCol,
} from "@coreui/react";
import { Auth } from "aws-amplify";
import React, { useReducer } from "react";

const initState = {
  oldPassword: "",
  newPassword: "",
  rePassword: "",
  isLoading: false,
  error: "",
};

function changePasswordReducer(state, action) {
  switch (action.type) {
    case "field":
      return {
        ...state,
        [action.field]: action.value,
      };
    case "change":
      return {
        ...state,
        isLoading: true,
      };
    case "success":
      return {
        ...state,
        oldPassword: "",
        newPassword: "",
        rePassword: "",
        isLoading: false,
        error: "",
      };
    case "error":
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}
function ChangePassword({ action, data }) {
  const [state, setState] = useReducer(changePasswordReducer, initState);
  const { oldPassword, newPassword, rePassword, isLoading, error } = state;

  const oldPasswordfield = (
    <CInputGroup className="mb-3">
      <CInputGroupPrepend>
        <CInputGroupText>
          <CIcon name="cil-lock-locked" />
        </CInputGroupText>
      </CInputGroupPrepend>
      <CInput
        type="password"
        required={true}
        value={oldPassword}
        onChange={(e) =>
          setState({
            type: "field",
            field: "oldPassword",
            value: e.target.value,
          })
        }
      />
    </CInputGroup>
  );

  const changePassword = () => {
    Auth.changePassword(data, oldPassword, newPassword).then((response) =>
      console.log(response)
    );
  };

  const updatePassword = () => {
    Auth.completeNewPassword(data, newPassword)
      .then((user) => {
        console.log(user);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <CRow>
        <CCard>
          <CCardBody>
            {action === "change_password" ? { oldPasswordfield } : ""}

            <CInputGroup className="mb-3">
              <CInputGroupPrepend>
                <CInputGroupText>
                  <CIcon name="cil-lock-locked" />
                </CInputGroupText>
              </CInputGroupPrepend>
              <CInput
                type="password"
                placeholder="New Password"
                required={true}
                value={newPassword}
                onChange={(e) =>
                  setState({
                    type: "field",
                    field: "newPassword",
                    value: e.target.value,
                  })
                }
              />
            </CInputGroup>
            <CInputGroup className="mb-4">
              <CInputGroupPrepend>
                <CInputGroupText>
                  <CIcon name="cil-lock-locked" />
                </CInputGroupText>
              </CInputGroupPrepend>
              <CInput
                type="password"
                placeholder="Re-Password"
                required={true}
                value={rePassword}
                onChange={(e) =>
                  setState({
                    type: "field",
                    field: "rePassword",
                    value: e.target.value,
                  })
                }
              />
            </CInputGroup>
            <CCol xs="6">
              <CButton
                color="primary"
                className="px-4"
                onClick={(e) => {
                  if (action === "change_password") {
                    changePassword();
                  } else if (action === "NEW_PASSWORD_REQUIRED") {
                    updatePassword();
                  }
                }}
              >
                Login
              </CButton>
            </CCol>
          </CCardBody>
        </CCard>
      </CRow>
    </div>
  );
}

export default ChangePassword;
