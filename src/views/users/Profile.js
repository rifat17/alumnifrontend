import React from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";

const Profile = () => {
  const auth = useSelector((state) => state.auth);
  const { username } = useParams();

  return (
    <div>
      <b>Profile of {username}</b>
    </div>
  );
};

export default Profile;
