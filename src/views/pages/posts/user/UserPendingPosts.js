import { CCard, CCardHeader, CCol, CRow } from "@coreui/react";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchPostsUSERPENDINGandDispatch, INDEXES, INREVIEW } from "src/actions/types";
import UserPendingPost from "./UserPendingPost";

function UserPendingPosts() {
  const authRedux = useSelector((state) => state.auth);
  const posts = useSelector((state) => state.post.pending)[authRedux.user.name];
  const data = useSelector((state) => state.post.pending);

  const dispatch = useDispatch();

  useEffect(() => {
    if (!data.loaded) {
      dispatch(fetchPostsUSERPENDINGandDispatch(INREVIEW, INDEXES.POST, "asc"));
    }
  }, []);

  return (
    <div>
      <CRow>
        <CCol xs="12" md="6">
          <CCard>
            {posts?.length > 0 ? (
              <CCardHeader>
                <b>Pending Posts...</b>
              </CCardHeader>
            ) : (
              <CCardHeader>
                <b>Under Dev...</b>
              </CCardHeader>
            )}

            {posts?.map((post) => (
              <UserPendingPost key={post.PK} data={post} />
            ))}
          </CCard>
        </CCol>
      </CRow>
    </div>
  );
}

export default UserPendingPosts;
