import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CForm,
  CFormGroup,
  CRow,
  CTextarea,
} from "@coreui/react";
import React, { useEffect, useReducer } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { ADD_POST_TO_HOME } from "src/actions/types";
import { EditPostRequest } from "src/services/post";

const initialData = {
  post: "",
  isLoading: false,
  error: "",
  new_content: "",
};

function editPostReducer(state, action) {
  switch (action.type) {
    case "EDITING":
      return {
        ...state,
        isLoading: true,
        error: "",
      };
    case "success":
      return {
        ...state,
        post: "",
        isLoading: false,
        error: "",
      };

    case "error":
      return {
        ...state,
        error: action.payload.error,
        isLoading: false,
      };
    case "field":
      return {
        ...state,
        [action.field]: action.value,
      };
    case "LOAD_POST":
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
}

function EditPost() {
  console.log("EDIT POST PAGE");

  const [postState, editPostDispatch] = useReducer(
    editPostReducer,
    initialData
  );
  const history = useHistory();
  const { ID } = useParams();
  const authRedux = useSelector((state) => state.auth);
  const postData = useSelector((state) => state.post.homePagePost).posts.find(
    (post) => {
      console.log(post);
      return post.ID === ID;
    }
  );

  const dispatch = useDispatch();
  const { jwtToken } = authRedux.user.session.idToken;
  // const dispatch = useDispatch();

  useEffect(() => {
    editPostDispatch({
      type: "LOAD_POST",
      payload: {
        new_content: postData.post_content,
        post: postData,
      },
    });
  }, []);

  console.log(postState);

  const handleOnSubmit = (e) => {
    e.preventDefault();
    editPostDispatch({ type: "EDITING" });

    console.log(postData);

    dispatch({
      type: "REMOVE_POST_FROM_HOME",
      payload: {
        PK: postData.PK,
      },
    });
    postState.post.post_content = postState.new_content;
    dispatch({
      type: ADD_POST_TO_HOME,
      payload: postState.post,
    });
    const update_post_model = {
      PK: postData.PK,
      SK: postData.PK,
      post_content: postState.new_content,
    };
    EditPostRequest(update_post_model, jwtToken);

    console.log("update_post_model", update_post_model);
    history.push("/");

    editPostDispatch({ type: "success" });
  };

  // const isEnabled = state.post.length > 0;

  //   console.log(post, postType, postTypeOptions, isLoading);
  return (
    <CRow>
      <CCol xs="12" md="6">
        <CCard>
          <CCardBody>
            <CForm onSubmit={handleOnSubmit}>
              <CFormGroup row>
                <CCol xs="12" md="9">
                  <CTextarea
                    name="textarea-input"
                    id="textarea-input"
                    rows="4"
                    placeholder={postState.new_content}
                    required={true}
                    value={postState.new_content}
                    onChange={(e) => {
                      editPostDispatch({
                        type: "field",
                        field: "new_content",
                        value: e.target.value,
                      });
                    }}
                  />
                </CCol>
              </CFormGroup>
              <CFormGroup>
                <CButton
                  disabled={postState.isLoading}
                  color="primary"
                  type="submit"
                >
                  {postState.isLoading ? "Posting.." : "Post"}
                </CButton>
              </CFormGroup>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
}

export default EditPost;
