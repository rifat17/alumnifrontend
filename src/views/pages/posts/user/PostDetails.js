import CIcon from "@coreui/icons-react";
import {
  CCard,
  CCardBody,
  CCardLink,
  CCardSubtitle,
  CCardText,
  CCardTitle,
} from "@coreui/react";
import React from "react";
import { useSelector } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import { getPostByID, INDEXES } from "src/actions/types";
import usePromise from "src/services/usePromise";
import moment from "moment";
import Moment from "react-moment";

function PostDetails() {
  console.log("Details POST PAGE");
  const history = useHistory();
  const authRedux = useSelector((state) => state.auth);

  const { ID } = useParams();

  const [data, error, isLoading] = usePromise(
    () => getPostByID(INDEXES.POST, ID).then((r) => r.hits.hits[0]._source),
    [ID]
  );
  console.log(data);
  const content = isLoading ? (
    "Loading.."
  ) : (
    <CCard>
      <CCardBody>
        <CCardTitle>
          {data.postOwner}
          {/* {data.postOwner.replace(/^\w/, (c) => c.toUpperCase())} */}
        </CCardTitle>
        <CCardSubtitle className="mb-2 text-muted">
          {data.cat_name} <CIcon />
          <Moment fromNow>
            {Math.round(new Date(+data.created_at).getTime())}
          </Moment>
        </CCardSubtitle>
        <div>
          <CCardText>{data.post_content}</CCardText>
          {data.postOwner === authRedux.user.name ? "Edit" : null}
          <CCardText>
            {data.file_ids.length > 0
              ? data.file_ids.map((file, idx) => (
                  <i style={{ margin: 2 }} key={`file-${idx}`}>
                    {`${idx + 1} ${file}`},
                  </i>
                ))
              : null}
          </CCardText>
          <CCardText>
            {"Tags: "}
            {data.tag_persons.length > 0
              ? data.tag_persons.map((person, idx) => (
                  <CCardLink
                    key={`link-${idx}`}
                    onClick={() => {
                      history.push(`/profile/${person}`);
                    }}
                  >
                    {person}
                  </CCardLink>
                ))
              : null}
          </CCardText>
        </div>
      </CCardBody>
    </CCard>
  );

  return <>{content}</>;
}

export default PostDetails;
