import CIcon from "@coreui/icons-react";
import {
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardLink,
  CCardSubtitle,
  CCardText,
  CCardTitle,
} from "@coreui/react";
import React from "react";
import Moment from "react-moment";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

import {
  ADD_POST_TO_HOME,
  REMOVE_POST_FROM_APPROVAL,
  REMOVE_POST_FROM_PENDING,
} from "src/actions/types";

function UserPendingPost({ data }) {
  const history = useHistory();
  const dispatch = useDispatch();
  console.log(data);

  const onEditHandler = (data) => {
    alert("Under dev...");
    // dispatch({
    //   type: REMOVE_POST_FROM_APPROVAL,
    //   PK: data.PK,
    // });
    // dispatch({
    //   type: ADD_POST_TO_HOME,
    //   payload: data,
    // });
    // dispatch({
    //   type: REMOVE_POST_FROM_PENDING,
    //   payload: {
    //     userPK: data.postOwner,
    //     postPK: data.PK,
    //   },
    // });
  };

  const onDeleteHandler = (data) => {
    dispatch({
      type: REMOVE_POST_FROM_APPROVAL,
      PK: data.PK,
    });

    dispatch({
      type: REMOVE_POST_FROM_PENDING,
      payload: {
        userPK: data.postOwner,
        postPK: data.PK,
      },
    });
  };

  // console.log("AdminPendingPost", data);
  return (
    <>
      <CCard>
        <CCardBody>
          <CCardTitle>
            {data.postOwner}
            {/* {data.postOwner.replace(/^\w/, (c) => c.toUpperCase())} */}
          </CCardTitle>
          <CCardSubtitle className="mb-2 text-muted">
            {data.cat_name} <CIcon />
            <Moment fromNow>{data.created_at}</Moment>
          </CCardSubtitle>

          <CCardText>{data.post_content}</CCardText>
          <CCardText>
            {data.file_ids.length > 0
              ? data.file_ids.map((file, idx) => (
                  <i style={{ margin: 2 }} key={`file-${idx}`}>
                    {`file-${idx} ${file}`}
                  </i>
                ))
              : null}
          </CCardText>
          <CCardText>
            {"Tags: "}
            {data.tag_persons.length > 0
              ? data.tag_persons.map((person, idx) => (
                  <CCardLink
                    key={`link-${idx}`}
                    onClick={() => {
                      history.push(`/profile/${person}`);
                    }}
                  >
                    {person}
                  </CCardLink>
                ))
              : null}
          </CCardText>

          <CButtonGroup role="group" aria-label="Basic example">
            <CButton
              className=""
              color="success"
              onClick={() => onEditHandler(data)}
            >
              Edit
            </CButton>
            <CButton
              className=""
              color="danger"
              onClick={() => onDeleteHandler(data)}
            >
              Delete
            </CButton>
          </CButtonGroup>
        </CCardBody>
      </CCard>
    </>
  );
}

export default UserPendingPost;
