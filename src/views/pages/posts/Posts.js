import { CCard, CCardHeader, CCol, CRow } from "@coreui/react";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { APPROVED, fetchPostsHOME, INDEXES } from "src/actions/types";
import Post from "./Post";
import PostForm from "./PostForm";

const Posts = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.post.homePagePost);
  console.log(data);

  React.useEffect(() => {
    if (!data.loaded) {
      dispatch(fetchPostsHOME(APPROVED, INDEXES.POST));
    }
  }, []);

  console.log(data);
  return (
    <>
      <PostForm />
      <CRow>
        <CCol xs="12" md="6">
          <CCard>
            <CCardHeader>
              <b>Recent Posts...</b>
            </CCardHeader>
            {data.posts.map((post) => (
              <Post key={post.PK} data={post} />
            ))}
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default Posts;
