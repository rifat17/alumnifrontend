const postData = [
  {
    PK: 1,
    postOwner: "hasib",
    SK: 1,
    post_id: 1,
    cat_id: 1,
    cat_name: "cat1",
    post_content: `  CCardBody,
    CCardTitle,
    CCardSubtitle,
    CCardText,`,
    file_ids: [1, 2, 3, 34, 34, 45, 67, 78],
    type: "type1",
    tag_persons: [],
    created_at: "datetime",
  },
  {
    PK: 2,
    postOwner: "hasib",
    SK: 2,
    post_id: 2,
    cat_id: 2,
    cat_name: "cat1",
    post_content: `{PK: 1, postOwner: "hasib", SK: 1, post_id: 1, cat_id: 1, …}
    Post.js:12 {PK: 2, postOwner: "hasib", SK: 2, post_id: 2, cat_id: 2, …}
    Post.js:12 {PK: 3, postOwner: "hasib2", SK: 3, post_id: 3, cat_id: 3, …}
    Post.js:12 {PK: 4, postOwner: "hasib", SK: 4, post_id: 4, cat_id: 4, …}`,
    file_ids: [1, 2, 3],
    type: "type1",
    tag_persons: ["t1", "t2"],
    created_at: "datetime",
  },
  {
    PK: 3,
    postOwner: "hasib2",
    SK: 3,
    post_id: 3,
    cat_id: 3,
    cat_name: "cat3",
    post_content:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.",
    file_ids: [1, 2, 3],
    type: "type1",
    tag_persons: ["t1", "t2", "x1", "x3"],
    created_at: "datetime",
  },
  {
    PK: 4,
    postOwner: "hasib",
    SK: 4,
    post_id: 4,
    cat_id: 4,
    cat_name: "cat1",
    post_content: "jsydfjsdyfsdjvsdjhcsdj",
    file_ids: [1, 2, 3],
    type: "type1",
    tag_persons: ["t1", "t2"],
    created_at: "datetime",
  },
];

export default postData;
