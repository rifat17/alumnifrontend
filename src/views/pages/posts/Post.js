import CIcon from "@coreui/icons-react";
import {
  CCard,
  CCardBody,
  CCardLink,
  CCardSubtitle,
  CCardText,
  CCardTitle,
} from "@coreui/react";
import moment from "moment";
import React from "react";
import Moment from "react-moment";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

const Post = ({ data }) => {
  const history = useHistory();
  const authRedux = useSelector((state) => state.auth);

  React.useEffect(() => {
    console.log("POST");
  }, []);
  // console.log(data);
  return (
    <>
      <CCard>
        <CCardBody>
          <CCardTitle>
            {data.postOwner}
            {/* {data.postOwner.replace(/^\w/, (c) => c.toUpperCase())} */}
          </CCardTitle>
          <CCardSubtitle className="mb-2 text-muted">
            {data.cat_name} <CIcon />
            <Moment fromNow>
              {Math.round(new Date(+data.created_at).getTime())}
            </Moment>
          </CCardSubtitle>
          <div>
            <CCardText>{data.post_content}</CCardText>
            {data.postOwner === authRedux.user.name ? (
              <CCardLink
                key={`link-${data.PK}`}
                onClick={() => {
                  history.push({
                    pathname: `/post/edit/${data.ID}`,
                    state: { ID: data.ID },
                  });
                }}
              >
                Edit
              </CCardLink>
            ) : null}
            <CCardText>
              {data.file_ids.length > 0
                ? data.file_ids.map((file, idx) => (
                    <i style={{ margin: 2 }} key={`file-${idx}`}>
                      {`${idx + 1} ${file}`},
                    </i>
                  ))
                : null}
            </CCardText>
            <CCardText>
              {"Tags: "}
              {data.tag_persons.length > 0
                ? data.tag_persons.map((person, idx) => (
                    <CCardLink
                      key={`link-${idx}`}
                      onClick={() => {
                        history.push(`/profile/${person}`);
                      }}
                    >
                      {person}
                    </CCardLink>
                  ))
                : null}
            </CCardText>
          </div>
        </CCardBody>
      </CCard>
    </>
  );
};

export default Post;

// <div className="card w-95" key={data.pk}>
//         <div className="card-body">
//           <h5 className="card-title m-0">
//             {data.postOwner.replace(/^\w/, (c) => c.toUpperCase())}
//           </h5>

//           <span className="badge badge-primary m-2 badge-dark">
//             {data.cat_name}
//           </span>
//           <small className="card-subtitle text-muted">{data.created_at}</small>
//           <p className="card-text">{data.post_content}</p>
//           {data.file_ids.length > 0 && (
//             <CCardGroup>{data.file_ids.map((file, idx) => file)}</CCardGroup>
//           )}
//           {data.tag_persons.length > 0 &&
//             data.tag_persons.map((person, idx) => (
//               <CCardLink key={`link-${idx}`} to={`/profile/${person}`}>
//                 {person}
//               </CCardLink>
//             ))}
//         </div>
//       </div>
