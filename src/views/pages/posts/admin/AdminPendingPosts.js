import { CCard, CCardHeader, CCol, CRow } from "@coreui/react";

import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchPostsADMINAPPROVAL, INDEXES, INREVIEW } from "src/actions/types";
import AdminPendingPost from "./AdminPendingPost";

const AdminPendingPosts = () => {
  const data = useSelector((state) => state.post.approve);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!data.loaded) {
      dispatch(fetchPostsADMINAPPROVAL(INREVIEW, INDEXES.POST, "asc"));
    }
  }, []);

  return (
    <div>
      <CRow>
        <CCol xs="12" md="6">
          <CCard>
            {data.posts.length > 0 ? (
              <CCardHeader>
                <b>Pending Posts...</b>
              </CCardHeader>
            ) : (
              <CCardHeader>
                <b>No Pending Posts...</b>
              </CCardHeader>
            )}
            {data.posts.map((post) => (
              <AdminPendingPost key={post.PK} data={post} />
            ))}
          </CCard>
        </CCol>
      </CRow>
    </div>
  );
};

export default AdminPendingPosts;
