import CIcon from "@coreui/icons-react";
import {
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardLink,
  CCardSubtitle,
  CCardText,
  CCardTitle,
} from "@coreui/react";
import React from "react";
import Moment from "react-moment";
import { useDispatch, useSelector } from "react-redux";

import {
  ADD_POST_TO_HOME,
  REMOVE_POST_FROM_APPROVAL,
  REMOVE_POST_FROM_PENDING,
} from "src/actions/types";
import { ApprovePost, RejectPost } from "src/services/post";

function AdminPendingPost({ data }) {
  const dispatch = useDispatch();
  const authRedux = useSelector((state) => state.auth);
  const { jwtToken } = authRedux.user.session.idToken;

  const onApproveHandler = (data) => {
    // const { jwtToken } = authRedux.user.session.idToken;
    ApprovePost(
      {
        PK: data.PK,
        SK: data.SK,
        postOwner: data.postOwner,
        tag_persons: data.tag_persons,
      },
      jwtToken
    );
    dispatch({
      type: REMOVE_POST_FROM_APPROVAL,
      PK: data.PK,
    });
    dispatch({
      type: ADD_POST_TO_HOME,
      payload: data,
    });
    // dispatch({
    //   type: REMOVE_POST_FROM_PENDING,
    //   payload: {
    //     userPK: data.postOwner,
    //     postPK: data.PK,
    //   },
    // });
  };

  const onRejectHandler = (data) => {
    RejectPost({ PK: data.PK, SK: data.SK }, jwtToken);
    dispatch({
      type: REMOVE_POST_FROM_APPROVAL,
      PK: data.PK,
    });

    // dispatch({
    //   type: REMOVE_POST_FROM_PENDING,
    //   payload: {
    //     userPK: data.postOwner,
    //     postPK: data.PK,
    //   },
    // });
  };

  // console.log("AdminPendingPost", data);
  return (
    <>
      <CCard>
        <CCardBody>
          <CCardTitle>
            {data.postOwner}
            {/* {data.postOwner.replace(/^\w/, (c) => c.toUpperCase())} */}
          </CCardTitle>
          <CCardSubtitle className="mb-2 text-muted">
            {data.cat_name} <CIcon />
            <Moment fromNow>{new Date(+data.created_at)}</Moment>
          </CCardSubtitle>

          <CCardText>{data.post_content}</CCardText>
          <CCardText>
            {data.file_ids.length > 0
              ? data.file_ids.map((file, idx) => (
                  <i style={{ margin: 2 }} key={`file-${idx}`}>
                    {`file-${idx} ${file}`}
                  </i>
                ))
              : null}
          </CCardText>
          <CCardText>
            {"Tags: "}
            {data.tag_persons.length > 0
              ? data.tag_persons.map((person, idx) => (
                  <CCardLink key={`link-${idx}`} to={`/profile/${person}`}>
                    {person}
                  </CCardLink>
                ))
              : null}
          </CCardText>
          <CButtonGroup role="group" aria-label="Basic example">
            <CButton
              className=""
              color="success"
              onClick={() => onApproveHandler(data)}
            >
              Approve
            </CButton>
            <CButton
              className=""
              color="danger"
              onClick={() => onRejectHandler(data)}
            >
              Reject
            </CButton>
          </CButtonGroup>
        </CCardBody>
      </CCard>
    </>
  );
}

export default AdminPendingPost;

// <CCardSubtitle className="mb-2 text-muted">
//             {data.cat_name} | {data.created_at ? data.created_at : "time"}
//           </CCardSubtitle>
//           <div>
//             <CCardText>
//               {data.post_content}
//               <div>
//                 {"files: "}
//                 {data.file_ids.length > 0
//                   ? data.file_ids.map((file, idx) => file)
//                   : null}
//               </div>
//               <div>
//                 {"Tags: "}
//                 {data.tag_persons.length > 0
//                   ? data.tag_persons.map((person, idx) => (
//                       <CCardLink key={`link-${idx}`} to={`/profile/${person}`}>
//                         {person}
//                       </CCardLink>
//                     ))
//                   : null}
//               </div>
//             </CCardText>
//           </div>
