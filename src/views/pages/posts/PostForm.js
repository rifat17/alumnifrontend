import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CForm,
  CFormGroup,
  CInputFile,
  CLabel,
  CRow,
  CSelect,
  CTextarea,
} from "@coreui/react";
import React, { useReducer } from "react";
import { useDispatch, useSelector } from "react-redux";
import { nanoid } from "nanoid";
import NameGenrator from "project-name-generator";

import {
  ADD_POST_TO_APPROVAL,
  ADD_POST_TO_PENDING,
  ADD_POST_TO_HOME,
} from "src/actions/types";
import moment from "moment";
import { POSTUserPostToDynamoDB } from "src/services/post";
const initialData = {
  post: "",
  category: "cat-1",
  categories: [
    {
      label: "Post",
      value: "cat-1",
    },
    {
      label: "Article",
      value: "cat-2",
    },
  ],
  files: [],
  isLoading: false,
  error: "",
};

function postReducer(state, action) {
  switch (action.type) {
    case "posting":
      return {
        ...state,
        isLoading: true,
        error: "",
      };
    case "success":
      return {
        ...state,
        post: "",
        files: [],
        category: "cat-1",
        isLoading: false,
        error: "",
      };

    case "error":
      return {
        ...state,
        error: action.payload.error,
        isLoading: false,
      };
    case "field":
      return {
        ...state,
        [action.field]: action.value,
      };
    case "files":
      return {
        ...state,
        files: action.payload,
      };

    default:
      return state;
  }
}

const get_cat_name = (cat_id) => {
  if (cat_id === "cat-1") {
    return "Category-1";
  } else if (cat_id === "cat-2") {
    return "Category-2";
  }
};

const construct_post = (data) => {
  const post = {};
  const ID = nanoid();

  post.PK = `POST#${ID}`;
  post.SK = `POST#${ID}`;
  post.type = "POST";
  post.ID = ID;
  post.cat_id = data.cat_id;
  post.cat_name = get_cat_name(data.cat_id);
  post.post_content = data.post;
  post.file_ids = [1, 2, 3].map((n) => nanoid());
  post.postOwner = data.postOwner;
  post.tag_persons = NameGenrator({ words: Math.floor(Math.random() * 5) }).raw;
  post.created_at = Date.now().toString();
  post.sub = data.sub;

  return post;
};

function PostForm() {
  // console.log(moment().format("YYYY/MM/DD HH:mm:ss").toString());
  const [state, postDispatch] = useReducer(postReducer, initialData);
  const authRedux = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const { post, category, categories, files, isLoading, error } = state;

  const handleOnSubmit = (e) => {
    e.preventDefault();
    postDispatch({ type: "posting" });

    const postData = {
      // file_ids: Array.from(files),
      cat_id: category,
      post: post,
      postOwner: authRedux.user.name,
      sub: authRedux.user.attributes.sub,
    };

    console.log(postData);
    const postWithMetadata = construct_post(postData);
    console.log(postWithMetadata);
    const { jwtToken } = authRedux.user.session.idToken;

    POSTUserPostToDynamoDB(postWithMetadata, jwtToken);
    async function postUserPost() {
      if (!authRedux.user.groups.includes("Admin")) {
        dispatch({
          type: ADD_POST_TO_APPROVAL,
          payload: postWithMetadata,
        });

        dispatch({
          type: ADD_POST_TO_PENDING,
          payload: {
            userPK: postWithMetadata.postOwner,
            post: postWithMetadata,
          },
        });
      } else {
        dispatch({
          type: ADD_POST_TO_HOME,
          payload: postWithMetadata,
        });
      }
    }

    postUserPost();
    postDispatch({ type: "success" });
  };

  const isEnabled = post.length > 0;

  //   console.log(post, postType, postTypeOptions, isLoading);
  return (
    <CRow>
      <CCol xs="12" md="6">
        <CCard>
          <CCardBody>
            <CForm onSubmit={handleOnSubmit}>
              <CFormGroup row>
                <CCol xs="12" md="9">
                  <CTextarea
                    name="textarea-input"
                    id="textarea-input"
                    rows="4"
                    placeholder="Whats on your mind?"
                    required={true}
                    value={post}
                    onChange={(e) => {
                      postDispatch({
                        type: "field",
                        field: "post",
                        value: e.target.value,
                      });
                    }}
                  />
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="9">
                  <CLabel htmlFor="select">Category</CLabel>
                  <CSelect
                    custom
                    name="select"
                    value={category}
                    onChange={(e) => {
                      postDispatch({
                        type: "field",
                        field: "category",
                        value: e.target.value,
                      });
                    }}
                  >
                    {categories.map((op, idx) => (
                      <option key={idx} value={op.value}>
                        {op.label}
                      </option>
                    ))}
                  </CSelect>
                </CCol>
              </CFormGroup>
              <CFormGroup row>
                <CCol xs="12" md="9">
                  {/* <CLabel>Multiple File input</CLabel> */}
                  <CInputFile
                    type="file"
                    id="file-multiple-input"
                    name="file-multiple-input"
                    accept="image/*"
                    multiple
                    onChange={(e) => {
                      postDispatch({
                        type: "files",
                        payload: e.target.files,
                      });
                    }}
                  />

                  <CLabel htmlFor="file-multiple-input" variant="custom-file">
                    Choose Files...
                  </CLabel>
                </CCol>
              </CFormGroup>
              <CFormGroup>
                <CButton disabled={isLoading} color="primary" type="submit">
                  {isLoading ? "Posting.." : "Post"}
                </CButton>
              </CFormGroup>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
}

export default PostForm;
