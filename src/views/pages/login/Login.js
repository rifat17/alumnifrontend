import React, { useReducer, useEffect } from "react";
import { Link, useHistory, useLocation } from "react-router-dom";
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useDispatch, useSelector } from "react-redux";
import { Auth } from "aws-amplify";

import {
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  USER_LOADING,
  USER_LOADED,
} from "src/actions/types";
import ChangePassword from "src/auth/password/ChangePassword";

const initState = {
  username: "hasib2",
  password: "123456",
  isLoading: false,
  error: "",
};
function loginReducer(state, action) {
  switch (action.type) {
    case "field":
      return {
        ...state,
        [action.field]: action.value,
      };
    case "login":
      return {
        ...state,
        isLoading: true,
      };
    case "success":
      return {
        ...state,
        username: "",
        password: "",
        isLoading: false,
        error: "",
      };
    case "error":
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}
const Login = () => {
  const dispatch = useDispatch();
  let history = useHistory();
  let location = useLocation();

  const auth = useSelector((state) => state.auth);
  const [localState, localDispatch] = useReducer(loginReducer, initState);
  const { username, password, isLoading, error } = localState;

  useEffect(() => {
    dispatch({
      type: USER_LOADING,
    });
    const isAuthenticated = async () => {
      try {
        const session = await Auth.currentSession();
        if (session) {
          const payload = session.accessToken.payload;
          const userDataToRedux = {
            name: payload.username,
            groups: payload["cognito:groups"] || [],
            attributes: payload,
            session,
          };
          dispatch({
            type: LOGIN_SUCCESS,
            payload: { user: userDataToRedux },
          });
          // const user = await Auth.currentAuthenticatedUser();
          // console.log(user);
          // console.log("LOCATION ", location.state);
          history.push(location.state ? location.state.prevLocation : "/");
        }
      } catch (error) {
        console.log(error);
      }

      dispatch({ type: USER_LOADED });
    };

    isAuthenticated();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    dispatch({
      type: USER_LOADING,
    });

    // const userdata = {
    //   username: username,
    //   password: password,
    // };

    console.log("USERNAME", username);
    try {
      let response = await Auth.signIn({
        username,
        password,
      });
      if (response.challengeName === "NEW_PASSWORD_REQUIRED") {
        response = await Auth.completeNewPassword(response, password);
      }
      console.log(response);

      const userDataToRedux = {
        name: response.username,
        groups:
          response.signInUserSession.accessToken.payload["cognito:groups"] ||
          [],
        attributes: response.attributes,
        session: response.signInUserSession,
      };
      // console.log(auth);
      dispatch({
        type: LOGIN_SUCCESS,
        payload: {
          user: userDataToRedux,
        },
      });
      localDispatch({
        type: "success",
      });
      history.push(location.state ? location.state.prevLocation : "/");
    } catch (error) {
      console.log(error);
      dispatch({
        type: LOGIN_FAIL,
        payload: {
          error: error.message ? error.message : "Login error",
        },
      });
      localDispatch({
        type: "error",
        payload: error.message ? error.message : "Login error",
      });
    }
  };

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="8">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm onSubmit={handleSubmit}>
                    <h1>Login</h1>
                    {error && (
                      <div
                        className="alert alert-danger fade show"
                        role="alert"
                      >
                        {error}
                      </div>
                    )}
                    <p className="text-muted">Sign In to your account</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type="text"
                        placeholder="Username"
                        autoComplete="username"
                        required={true}
                        value={username}
                        onChange={(e) =>
                          localDispatch({
                            type: "field",
                            field: "username",
                            value: e.target.value,
                          })
                        }
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type="password"
                        placeholder="Password"
                        autoComplete="current-password"
                        required={true}
                        value={password}
                        onChange={(e) =>
                          localDispatch({
                            type: "field",
                            field: "password",
                            value: e.target.value,
                          })
                        }
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs="6">
                        <CButton color="primary" className="px-4" type="submit">
                          Login
                        </CButton>
                      </CCol>
                      <CCol xs="6" className="text-right">
                        <CButton color="link" className="px-0">
                          Forgot password?
                        </CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard
                className="text-white bg-primary py-5 d-md-down-none"
                style={{ width: "44%" }}
              >
                <CCardBody className="text-center">
                  <div>
                    <h2>Sign up</h2>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua.
                    </p>
                    <Link to="/register">
                      <CButton
                        color="primary"
                        className="mt-3"
                        active
                        tabIndex={-1}
                      >
                        Register Now!
                      </CButton>
                    </Link>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Login;
