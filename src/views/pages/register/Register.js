import CIcon from "@coreui/icons-react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CCardFooter,
} from "@coreui/react";
import { Auth } from "aws-amplify";
import React, { useReducer } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, Link } from "react-router-dom";
import {
  REGISTRATION,
  REGISTRATION_FAIL,
  REGISTRATION_SUCCESS,
} from "src/actions/types";

const initState = {
  username: "hasib2",
  email: "abdullah.qups@gmail.com",
  password: "123456",
  confirm_password: "123456",
  isLoading: false,
  error: "",
};
function registerReducer(state, action) {
  switch (action.type) {
    case "field":
      return {
        ...state,
        [action.field]: action.value,
      };
    case "signup":
      return {
        ...state,
        isLoading: true,
      };
    case "success":
      return {
        ...state,
        username: "",
        password: "",
        email: "",
        isLoading: false,
        error: "",
      };
    case "error":
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}

const Register = () => {
  const dispatch = useDispatch();
  let history = useHistory();

  const auth = useSelector((state) => state.auth);
  const [localState, localDispatch] = useReducer(registerReducer, initState);
  const { username, email, password, confirm_password, isLoading, error } =
    localState;

  const handleSubmit = async (e) => {
    e.preventDefault();
    dispatch({
      type: REGISTRATION,
    });

    const user = {
      username: username,
      email,
      password: password,
      confirm_password: confirm_password,
    };
    console.log(user);

    try {
      const response = await Auth.signUp({
        username,
        password,
        attributes: {
          email,
        },
      });
      console.log(response);

      const auth = {
        name: response.user.username,
        sub: response.userSub,
      };
      // console.log(user, userConfirmed, userSub);
      dispatch({
        type: REGISTRATION_SUCCESS,
        payload: { user: auth },
      });
      localDispatch({
        type: "success",
      });
      history.push({
        pathname: "/confirm-email",
        state: { email, username },
      });
    } catch (error) {
      console.log(error);
      dispatch({
        type: REGISTRATION_FAIL,
        payload: {
          error: error.message ? error.message : "Registration error",
        },
      });
      localDispatch({
        type: "error",
        payload: error.message ? error.message : "Registration error",
      });
    }
  };

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="9" lg="7" xl="6">
            <CCard className="mx-4">
              <CCardBody className="p-4">
                <CForm onSubmit={handleSubmit}>
                  <h1>Register</h1>
                  {error && (
                    <div className="alert alert-danger fade show" role="alert">
                      {error}
                    </div>
                  )}
                  <p className="text-muted">Create your account</p>
                  <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        <CIcon name="cil-user" />
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      type="text"
                      placeholder="Username"
                      autoComplete="username"
                      required={true}
                      value={username}
                      onChange={(e) =>
                        localDispatch({
                          type: "field",
                          field: "username",
                          value: e.target.value,
                        })
                      }
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>@</CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      type="text"
                      placeholder="Email"
                      autoComplete="email"
                      required={true}
                      value={email}
                      onChange={(e) =>
                        localDispatch({
                          type: "field",
                          field: "email",
                          value: e.target.value,
                        })
                      }
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        <CIcon name="cil-lock-locked" />
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      type="password"
                      placeholder="Password"
                      autoComplete="new-password"
                      required={true}
                      value={password}
                      onChange={(e) =>
                        localDispatch({
                          type: "field",
                          field: "password",
                          value: e.target.value,
                        })
                      }
                    />
                  </CInputGroup>
                  {/* <CInputGroup className="mb-4">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        <CIcon name="cil-lock-locked" />
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      type="password"
                      placeholder="Repeat password"
                      autoComplete="new-password"
                      required={true}
                      value={confirm_password}
                      onChange={(e) =>
                        localDispatch({
                          type: "field",
                          field: "confirm_password",
                          value: e.target.value,
                        })
                      }
                    />
                  </CInputGroup> */}
                  <CButton
                    type="submit"
                    disabled={auth.isLoading}
                    color="success"
                    block
                  >
                    {auth.isLoading ? "Creating Account..." : "Create Account"}
                  </CButton>
                </CForm>
              </CCardBody>

              <CCardFooter className="p-4">
                <CRow>
                  <CCol xs="12" sm="20">
                    <Link to="/login">
                      <CButton
                        color="primary"
                        className="mt-0"
                        block
                        tabIndex={-1}
                      >
                        Login
                      </CButton>
                    </Link>
                  </CCol>
                </CRow>
              </CCardFooter>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Register;
