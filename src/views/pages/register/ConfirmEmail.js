import {
  CRow,
  CCol,
  CCard,
  CCardBody,
  CCardHeader,
  CButton,
} from "@coreui/react";
import React from "react";
import { useLocation, useHistory } from "react-router-dom";

function ConfirmEmail() {
  const location = useLocation();
  const history = useHistory();

  return (
    <>
      <CRow className="justify-content-center">
        <CCol xs="12" sm="12" md="6">
          <CCard borderColor="success">
            <CCardHeader>
              Welcome <b>{location.state.username}</b>
            </CCardHeader>
            <CCardBody>
              Thanks for signing up. Please check your email{" "}
              {location.state.email && <b>{location.state.email}</b>} for
              confirmation!
            </CCardBody>
            <CButton color="success" onClick={() => history.push("/login")}>
              Login
            </CButton>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
}

export default ConfirmEmail;
