import CIcon from "@coreui/icons-react";
import {
  CButton,
  CHeader,
  CHeaderBrand,
  CHeaderNav,
  CHeaderNavItem,
  CHeaderNavLink,
  CToggler,
} from "@coreui/react";
import { Auth } from "aws-amplify";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  fetchPostsUSERPENDING,
  INDEXES,
  INREVIEW,
  LOGOUT_SUCCESS,
} from "src/actions/types";

const load = async (user) => {
  try {
    const data = await fetchPostsUSERPENDING(INREVIEW, INDEXES.POST, user);
    console.log("ASYNC", data);
    return data;
  } catch {}
};

const TheHeader = () => {
  let history = useHistory();
  const dispatch = useDispatch();
  const sidebarShow = useSelector((state) => state.sidebarShow.sidebarShow);
  const authRedux = useSelector((state) => state.auth);
  const adminApprovePost = useSelector((state) => state.post.approve.posts);
  const userPendinPost = useSelector((state) => state.post.pending)[
    authRedux.user.name
  ];

  // console.log("adminApprovePost", adminApprovePost);
  // console.log("userPendinPost", userPendinPost);

  const toggleSidebar = () => {
    const val = [true, "responsive"].includes(sidebarShow)
      ? false
      : "responsive";
    dispatch({ type: "set", sidebarShow: val });
  };

  const toggleSidebarMobile = () => {
    const val = [false, "responsive"].includes(sidebarShow)
      ? true
      : "responsive";
    dispatch({ type: "set", sidebarShow: val });
  };
  let currentUser, currentUserGroups;
  if (authRedux.user) {
    currentUser = authRedux.user.name;
    currentUserGroups = authRedux.user.groups;
  }

  const user_posts = load(currentUser);
  console.log("user_posts", user_posts);

  const adminHeader = () => (
    <>
      <CHeaderNav className="d-md-down-none mr-auto">
        <CHeaderNavItem className="px-3">
          <CHeaderNavLink onClick={() => history.push(`/approve-posts`)}>
            Approve Posts({adminApprovePost?.length})
          </CHeaderNavLink>
        </CHeaderNavItem>

        <CHeaderNavItem className="px-3">
          <CHeaderNavLink onClick={() => history.push("/")}>
            Home
          </CHeaderNavLink>
        </CHeaderNavItem>
        <CHeaderNavItem className="px-3">
          <CHeaderNavLink
            onClick={() => history.push(`/profile/${authRedux.user.name}`)}
          >
            Profile
          </CHeaderNavLink>
        </CHeaderNavItem>
        <CHeaderNavItem className="px-3">
          <CHeaderNavLink onClick={() => history.push(`/users`)} to="/users">
            Users
          </CHeaderNavLink>
        </CHeaderNavItem>
        {/* <CHeaderNavItem className="px-3">
          <CHeaderNavLink>Settings</CHeaderNavLink>
        </CHeaderNavItem> */}
      </CHeaderNav>
    </>
  );

  const userHeader = () => (
    <>
      <CHeaderNav className="d-md-down-none mr-auto">
        <CHeaderNavItem className="px-3">
          <CHeaderNavLink onClick={() => history.push(`/pending-posts`)}>
            Pending Post {user_posts?.length}
          </CHeaderNavLink>
        </CHeaderNavItem>
        <CHeaderNavItem className="px-3">
          <CHeaderNavLink onClick={() => history.push("/")}>
            Home
          </CHeaderNavLink>
        </CHeaderNavItem>
        <CHeaderNavItem className="px-3">
          <CHeaderNavLink
            onClick={() => history.push(`/profile/${authRedux.user.name}`)}
          >
            Profile
          </CHeaderNavLink>
        </CHeaderNavItem>
        {/* <CHeaderNavItem className="px-3">
          <CHeaderNavLink>Settings</CHeaderNavLink>
        </CHeaderNavItem> */}
      </CHeaderNav>
    </>
  );
  const header = () => {
    if (currentUserGroups?.includes("Admin")) {
      return adminHeader();
    } else {
      return userHeader();
    }
  };
  return (
    <CHeader withSubheader>
      <CToggler
        inHeader
        className="ml-md-3 d-lg-none"
        onClick={toggleSidebarMobile}
      />
      <CToggler
        inHeader
        className="ml-3 d-md-down-none"
        onClick={toggleSidebar}
      />
      <CHeaderBrand className="mx-auto d-lg-none" to="/">
        <CIcon name="logo" height="48" alt="Logo" />
      </CHeaderBrand>
      {authRedux.isLoading ? "Loading.." : header()}
      <CHeaderNav className="px-3">
        <CButton
          onClick={async () => {
            await Auth.signOut();
            dispatch({ type: LOGOUT_SUCCESS });
            history.push("/");
          }}
          color="primary"
          className="px-4"
        >
          Logout
        </CButton>
      </CHeaderNav>
      {/* <CHeaderNav className="px-3">
        <TheHeaderDropdownNotif />
        <TheHeaderDropdownTasks />
        <TheHeaderDropdownMssg />
        <TheHeaderDropdown />
      </CHeaderNav> */}
    </CHeader>
  );
};

export default TheHeader;

/* <CSubheader className="px-3 justify-content-between">
        <CBreadcrumbRouter
          className="border-0 c-subheader-nav m-0 px-0 px-md-3"
          routes={routes}
        />
        <div className="d-md-down-none mfe-2 c-subheader-nav">
          <CLink className="c-subheader-nav-link" href="#">
            <CIcon name="cil-speech" alt="Settings" />
          </CLink>
          <CLink
            className="c-subheader-nav-link"
            aria-current="page"
            to="/dashboard"
          >
            <CIcon name="cil-graph" alt="Dashboard" />
            &nbsp;Dashboard
          </CLink>
          <CLink className="c-subheader-nav-link" href="#">
            <CIcon name="cil-settings" alt="Settings" />
            &nbsp;Settings
          </CLink>
        </div>
      </CSubheader> */
