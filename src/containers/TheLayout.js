import React from "react";
import Posts from "src/views/pages/posts/Posts";
import { TheContent, TheSidebar, TheFooter, TheHeader } from "./index";

const TheLayout = () => {
  return (
    <div className="c-app c-default-layout">
      <TheSidebar />
      <div className="c-wrapper">
        <TheHeader />
        <div className="c-body">
          <TheContent />
          {/* <Posts /> */}
        </div>
        <TheFooter />
      </div>
    </div>
  );
};

export default TheLayout;
